package com.example.navigationview2023

import android.os.Bundle
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.activity.OnBackPressedDispatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.navigationview2023.databinding.ActivityMainBinding
import com.example.navigationview2023.databinding.CabeceraBinding
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // navController
        navController = findNavController(R.id.fragmentContainerView)
        //appBarConfiguration con las IDs de los menús/fragments y el drawerlayout
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.home, R.id.video, R.id.gallery, R.id.slideshow, R.id.sports
            ), binding.drawerlayout
        )
        //sincronización de la barra superior con la navegación
        setupActionBarWithNavController(navController, appBarConfiguration)
        //sincronización del menú lateral con la navegación
        binding.navigationview.setupWithNavController(navController)

        //sobreescribir botón atrás físico
        onBackPressedDispatcher.addCallback(this, object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                // si el menú está abierto, lo cerramos
                if (binding.drawerlayout.isDrawerOpen(GravityCompat.START)) {
                    binding.drawerlayout.closeDrawer(GravityCompat.START)
                } else {
                    // la función navigateUp navega hacia atrás en la navegación y devuelve un boolean
                    // si no se ha podido navegar, es porque estamos en el fragment principal
                    // si es así, se cierra la app
                    if (!navController.navigateUp())
                        finish()
                }
            }
        })

        // todo acceder a las vistas del header, cuando se ha añadido el header por XML
        val header = binding.navigationview.getHeaderView(0)
        val textview = header.findViewById<TextView>(R.id.tv1)
        textview.text = "Título"

        // todo añadir el header por código
        /*
        val headerBinding = CabeceraBinding.inflate(layoutInflater)
        binding.navigationview.addHeaderView(headerBinding.root)
        headerBinding.tv1.text = "Titulo2"
         */
    }

    /**
     * Función para que funcion el menú de hamburguesa y el botón atrás de la barra superior.
     */
    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}