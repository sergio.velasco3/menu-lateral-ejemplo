package com.example.navigationview2023

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.navigationview2023.databinding.FragmentSportBinding
import com.example.navigationview2023.databinding.FragmentVideoBinding

class SportFragment : Fragment() {

    private lateinit var binding: FragmentSportBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentSportBinding.inflate(inflater, container, false)
        return binding.root
    }

}